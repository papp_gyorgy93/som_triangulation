part of simulation;

class Route{

  List<Edge> edges;

  Route(this.edges){

  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    var retVal = true;
    for(Edge e in this.edges){
      if(!(other as Route).edges.contains(e)){
        retVal = false;
        break;
      }
    }
    return retVal;
  }

  @override
  int get hashCode {
    return edges.hashCode;
  }


}