part of simulation;

class Som{
  List<Vector3> input;
  List<Node> map;
  Scene sc;

  double eb, en, lambda, alpha, beta;
  int ageMax;

  int numberOfInput = 0;

  Object3D meshOfEpsilon;
  Object3D meshOfs1;
  Object3D meshOfs2;
  Object3D meshOfs3;

  //stop criterion - number of nodes in the map
  int nv;

  Som(this.input, Scene sc){

    //set parameters:
    lambda = 400.0;
    eb = 0.05;
    en = 0.0006;
    alpha = 0.5;
    beta = 0.0005;
    ageMax = 30;
    nv = 20;

    this.sc = sc;
    this.map = new List<Node>();
    var randomPoint = new List();
    var rand = new Math.Random();
    while(randomPoint.length < 3){
      var newValue = rand.nextInt(input.length);
      if(!randomPoint.contains(newValue)){
        randomPoint.add(newValue);
      }
    }
    this.map.add(new Node(this.input[randomPoint[0]]));
    this.map.add(new Node(this.input[randomPoint[1]]));
    this.map.add(new Node(this.input[randomPoint[2]]));
    sc.add(this.map[0].mesh);
    sc.add(this.map[1].mesh);
    sc.add(this.map[2].mesh);

    //var a = new Edge(this.map[0], this.map[1]);
    //var b = new Edge(this.map[0], this.map[2]);
    //var c = new Edge(this.map[1], this.map[2]);
    //var face = new MFace(a,b,c);
    //scene.add(a.mesh);
    //scene.add(b.mesh);
    //scene.add(c.mesh);

    var geomEpsilon = new Geometry();
    geomEpsilon.vertices.add(new Vector3.zero());
    geomEpsilon.isDynamic = true;
    geomEpsilon.verticesNeedUpdate = true;
    meshOfEpsilon = new ParticleSystem(geomEpsilon, new ParticleBasicMaterial(color: 0x00ff00, size: 15));
    meshOfEpsilon.isDynamic = true;

    var geom_s1 = new Geometry();
    geom_s1.vertices.add(new Vector3.zero());
    geom_s1.isDynamic = true;
    geom_s1.verticesNeedUpdate = true;
    meshOfs1 = new ParticleSystem(geom_s1, new ParticleBasicMaterial(color: 0x0000ff, size: 10));
    meshOfs1.isDynamic = true;

    var geom_s2 = new Geometry();
    geom_s2.vertices.add(new Vector3.zero());
    geom_s2.isDynamic = true;
    geom_s2.verticesNeedUpdate = true;
    meshOfs2 = new ParticleSystem(geom_s2, new ParticleBasicMaterial(color: 0xffff00, size: 10));
    meshOfs2.isDynamic = true;

    var geom_s3 = new Geometry();
    geom_s3.vertices.add(new Vector3.zero());
    geom_s3.isDynamic = true;
    geom_s3.verticesNeedUpdate = true;
    meshOfs3 = new ParticleSystem(geom_s3, new ParticleBasicMaterial(color: 0x00ffff, size: 10));
    meshOfs3.isDynamic = true;

    scene.add(meshOfEpsilon);
    scene.add(meshOfs1);
    scene.add(meshOfs2);
    //scene.add(meshOfs3);

  }

  bool isLearning(){
    return (nv+1) > map.length;
  }

  void updatePointMesh(Vector3 epsilon, Node s1, Node s2, [Node s3 = null]){
    meshOfEpsilon.geometry.vertices[0] = epsilon;
    meshOfEpsilon.geometry.verticesNeedUpdate = true;

    meshOfs1.geometry.vertices[0] = s1.weight;
    meshOfs1.geometry.verticesNeedUpdate = true;

    meshOfs2.geometry.vertices[0] = s2.weight;
    meshOfs2.geometry.verticesNeedUpdate = true;
    if(s3 != null){
      meshOfs3.geometry.vertices[0] = s3.weight;
      meshOfs3.geometry.verticesNeedUpdate = true;
    }

  }

  void updateMeshPoint(){
    for(Node n in map){
      n.updateMesh();
    }
  }

  int step = 0;

  var epsilon;

  var closestNodes;

  bool isOnce = true;

  bool isLearningFinished = false;

  void buildFaceCreation(){
    map = new List();


    // 0 - 7 - 6
    // |   |   | \
    // 1 - 9 - 5 - 8
    // |   |   | /
    // 2 - 3 - 4

    map.add(new Node(new Vector3(-50.0, 50.0, 0.0))); //0
    map.add(new Node(new Vector3(-50.0, 0.0, 0.0))); //1
    map.add(new Node(new Vector3(-50.0, -50.0, 0.0))); //2
    map.add(new Node(new Vector3(0.0, -50.0, 0.0))); //3
    map.add(new Node(new Vector3(50.0, -50.0, 0.0))); //4
    map.add(new Node(new Vector3(50.0, 0.0, 0.0))); //5
    map.add(new Node(new Vector3(50.0, 50.0, 0.0))); //6
    map.add(new Node(new Vector3(0.0, 50.0, 0.0))); //7
    map.add(new Node(new Vector3(0.0, 0.0, -50.0))); //8
    map.add(new Node(new Vector3(0.0, 0.0, 0.0))); //9

    var a = new Edge(map[0], map[1]);
    var b = new Edge(map[1], map[2]);
    var c = new Edge(map[2], map[3]);
    var d = new Edge(map[3], map[4]);
    var e = new Edge(map[4], map[5]);
    var f = new Edge(map[5], map[6]);
    var g = new Edge(map[6], map[7]);
    var h = new Edge(map[0], map[7]);
    var i = new Edge(map[1], map[9]);
    var j = new Edge(map[3], map[9]);
    var k = new Edge(map[5], map[9]);
    var l = new Edge(map[7], map[9]);
    var m = new Edge(map[0], map[8]);
    var n = new Edge(map[1], map[8]);
    var o = new Edge(map[2], map[8]);
    var p = new Edge(map[3], map[8]);
    var q = new Edge(map[4], map[8]);
    var r = new Edge(map[5], map[8]);
    var s = new Edge(map[6], map[8]);
    var t = new Edge(map[7], map[8]);

    scene.add(a.mesh);
    scene.add(b.mesh);
    scene.add(c.mesh);
    scene.add(d.mesh);
    scene.add(e.mesh);
    scene.add(f.mesh);
    scene.add(g.mesh);
    scene.add(h.mesh);
    scene.add(i.mesh);
    scene.add(j.mesh);
    scene.add(k.mesh);
    scene.add(l.mesh);
    scene.add(m.mesh);
    scene.add(n.mesh);
    scene.add(o.mesh);
    scene.add(p.mesh);
    scene.add(q.mesh);
    scene.add(r.mesh);
    scene.add(s.mesh);
    scene.add(t.mesh);

    var aa = new MFace(a, m, n);
    var ab = new MFace(b, n, o);
    var ac = new MFace(c, o, p);
    var ad = new MFace(d, p, q);
    var ae = new MFace(e, q, r);
    var af = new MFace(f, r, s);
    var ag = new MFace(g, s, t);
    var ah = new MFace(h, t, m);

    scene.add(aa.mesh);
    scene.add(ab.mesh);
    scene.add(ac.mesh);
    scene.add(ad.mesh);
    scene.add(ae.mesh);
    scene.add(af.mesh);
    scene.add(ag.mesh);
    scene.add(ah.mesh);
  }

  void testFaceCreation(){
    fillQuads();
  }

  void learning(){
    var rand = new Math.Random();

    if(isLearning()){

      numberOfInput++;

      //randomly chosen point from pount cloud

      if(step == 0){

        epsilon = input[rand.nextInt(input.length)];
        closestNodes = findTheTwoBestMatchingNode(epsilon);
        updatePointMesh(epsilon, closestNodes[0], closestNodes[1]);

        step++;
        return;
      }else{
        step = 0;
      }

      createConnectionAndECHL(closestNodes);

      thalesCircleRemove(closestNodes.first, closestNodes.last);

      closestNodes.first.updateErrorCounter(epsilon);

      closestNodes.first.moveTowards(epsilon, eb, en);

      closestNodes.first.updateAges();

      edgeRemove();

      insertEdge();

      decreaseAllNodeError();

      updateMeshPoint();

      if(!isLearning()){
        var list = new List();
        for(Object3D o in scene.children){
          if(o.name == "input"){
            list.add(o);
          }
        }
        for(Object3D o in list){
          scene.remove(o);
        }


        for(Vector3 v in input){
          closestNodes = findTheTwoBestMatchingNode(v);
          createConnectionAndECHL(closestNodes);
        }
        var alam = 0;
      }


    }else{
      /*var edges = Edge.getAllEdge(map);
      for(var i = 0; i < edges.length; i++){
        Edge.removeEdge(edges[i].start, edges[i].end, scene);
      }*/
      //completeTopologicalLearning();

      for(Vector3 v in input){
        closestNodes = findTheTwoBestMatchingNode(v);
        createConnectionAndECHLThree(closestNodes);
      }


    }

    //completeTopologicalLearning();
    print("map length: ${map.length}");
  }

  int topologicallLearningIndex = 0;

  void completeTopologicalLearning(){

    if(topologicallLearningIndex < input.length){
      if(step == 0) {
        epsilon = input[topologicallLearningIndex];
        closestNodes = findTheThreeBestMatchingNode(epsilon);
        updatePointMesh(
            epsilon, closestNodes[0], closestNodes[1], closestNodes[2]);

        step++;
        return;
      }else{
        step = 0;
      }


      Edge connection = Edge.isConnected(closestNodes[0], closestNodes[1]);

      if(connection != null){
        connection.age = 0;
      }else{
        var edgeBetweenClosestPoint = new Edge(closestNodes[0], closestNodes[1]);
        scene.add(edgeBetweenClosestPoint.mesh);
      }

      Edge c_s1_s3 = Edge.isConnected(closestNodes[0], closestNodes[2]);
      Edge c_s2_s3 = Edge.isConnected(closestNodes[1], closestNodes[2]);

      if(c_s1_s3 != null && c_s2_s3 != null){
        if(!delaunayTest(connection)){
          MFace.removeEdgeAllFace(connection, scene);
        }
        if(!delaunayTest(c_s1_s3)){
          MFace.removeEdgeAllFace(c_s1_s3, scene);
        }
        if(!delaunayTest(c_s2_s3)){
          MFace.removeEdgeAllFace(c_s2_s3, scene);
        }
        if(connection.numberOfFace < 2 &&
            c_s1_s3.numberOfFace < 2 && c_s2_s3.numberOfFace < 2 ){
          var isFaceExists = false;
          connection.faces.forEach((MFace face){
            if(face.isContainEdge(c_s1_s3) && face.isContainEdge(c_s2_s3)){
              isFaceExists = true;
            }
          });
          if(!isFaceExists){
            MFace newFace = new MFace(connection, c_s1_s3, c_s2_s3);
            scene.add(newFace.mesh);
          }
        }
      }

    }
    topologicallLearningIndex++;


  }

  bool delaunayTest(Edge edge){
    if(edge.numberOfFace == 1){
      return true;
    }
    var a = (edge.start.weight - edge.end.weight).length;
    var otherSides = new List<double>();
    var cotangensValues = new List<double>();
    for(var i = 0; i < edge.faces.length; i++){
      for(Edge e in edge.faces[i].edges){
        if(e != edge){
          otherSides.add(e.length);
        }
      }
      var b = otherSides[0];
      var c = otherSides[1];
      var tanAlphaPer2 = Math.sqrt( ((a-b+c)*(a+b+c)) / ((a+b+c)*(-a+b+c)) );
      cotangensValues.add( (1 - Math.pow(tanAlphaPer2, 2))/( 2 * tanAlphaPer2) );
    }

    var weight_ij = cotangensValues.first + cotangensValues.last;

    return weight_ij > 0;
  }

  void decreaseAllNodeError(){
    for(Node n in map){
      n.updateErrorVariable(beta);
    }
  }

  void removeVertex(Node a){
    if(a.listOfEdges.length < 0){
      map.remove(a);
      scene.remove(a.mesh);
    }
  }

  void edgeRemove(){
    var listOfEdges = new List.from(Edge.getAllEdge(map));

    var removeObj = new List();

    for(Edge e in listOfEdges){
      if(e.age > ageMax){
        MFace.removeFace(e, scene);
        Edge.removeEdge(e.start, e.end, scene);
        removeVertex(e.start);
        removeVertex(e.end);
      }
    }
    for(Edge e in listOfEdges) {
      if (e.faces.length < 1) {
        removeObj.add(e);
      }
    }

    /*for(Edge e in removeObj){
      Edge.removeEdge(e.start, e.end, scene);
    }*/

    /*var removeObj = new List();
    for(Node n in map){
      if(n.listOfEdges.length < 1){
        removeObj.add(n);
      }
    }

    for(Node obj in removeObj){
      map.remove(obj);
      scene.remove(obj.mesh);
    }*/
  }

  void thalesCircleRemove(Node s1, Node s2){
    var s1_neighbours = s1.getNeighbours();
    var u = s1.weight - s2.weight;
    for(Node n in s1_neighbours){
      if(n != s2){
        var v = n.weight - s2.weight;
        if(u.dot(v) < 0){
          Edge removeEdge = Edge.isConnected(s1,n);
          MFace.removeFace(removeEdge, scene);
          Edge.removeEdge(s1,n, scene);
          removeVertex(removeEdge.start);
          removeVertex(removeEdge.end);
        }
      }
    }
  }

  void finalThalesCircleRemove(){
    var listOfEdges = Edge.getAllEdge(this.map);
    for(Edge e in listOfEdges){
      thalesCircleRemove(e.start, e.end);
    }
  }

  void insertEdge(){
    if(numberOfInput > lambda){
      if(nv == map.length){
        nv--;
        return;
      }
      var highestErrorNodes = Node.getHighestErrorNodes(map);
      var edgeOfHighestErrorNodes = Edge.isConnected(
          highestErrorNodes.first, highestErrorNodes.last);

      Edge.removeEdge(edgeOfHighestErrorNodes.start, edgeOfHighestErrorNodes.end, scene);

      Node sr = new Node(
          (highestErrorNodes.first.weight + highestErrorNodes.last.weight).scale(0.5)
      );

      this.map.add(sr);
      sc.add(sr.mesh);

      var edge_sr_sq = new Edge(highestErrorNodes.first, sr);
      var edge_sr_sf = new Edge(highestErrorNodes.last, sr);

      scene.add(edge_sr_sq.mesh);
      scene.add(edge_sr_sf.mesh);

      /*var commonNeighbours = Node.getCommonNeighbours(highestErrorNodes.first, highestErrorNodes.last);

      MFace.removeFace(edgeOfHighestErrorNodes, scene);

      Edge.removeEdge(edgeOfHighestErrorNodes.start, edgeOfHighestErrorNodes.end, scene);

      Node sr = new Node(
          (highestErrorNodes.first.weight + highestErrorNodes.last.weight).scale(0.5)
      );

      this.map.add(sr);
      sc.add(sr.mesh);

      var edge_sr_sq = new Edge(highestErrorNodes.first, sr);
      var edge_sr_sf = new Edge(highestErrorNodes.last, sr);

      var edge_sr_c1 = new Edge(commonNeighbours.first, sr);

      var edge_sq_c1 = Edge.isConnected(highestErrorNodes.first, commonNeighbours.first);
      if(edge_sq_c1 == null){
        edge_sq_c1 = new Edge(highestErrorNodes.first, commonNeighbours.first);
      }

      var edge_sf_c1 = Edge.isConnected(highestErrorNodes.last, commonNeighbours.first);
      if(edge_sf_c1 == null){
        edge_sf_c1 = new Edge(highestErrorNodes.last, commonNeighbours.first);
      }

      if(commonNeighbours.length > 2){
        //Do not do anything
        //throw new StateError("More than two common neighbours");

        var edge_sr_c2 = new Edge(commonNeighbours[1], sr);
        var edge_sr_c3 = new Edge(commonNeighbours[2], sr);

        var edge_sq_c2 = Edge.isConnected(highestErrorNodes.first, commonNeighbours[1]);
        var edge_sf_c2 = Edge.isConnected(highestErrorNodes.last, commonNeighbours[1]);

        var edge_sq_c3 = Edge.isConnected(highestErrorNodes.first, commonNeighbours[2]);
        var edge_sf_c3 = Edge.isConnected(highestErrorNodes.last, commonNeighbours[2]);

        MFace face = new MFace(edge_sr_sq, edge_sr_c1, edge_sq_c1);
        MFace face1 = new MFace(edge_sr_sq, edge_sr_c2, edge_sq_c2);
        MFace face2 = new MFace(edge_sr_sf, edge_sr_c1, edge_sf_c1);
        MFace face3 = new MFace(edge_sr_sf, edge_sr_c2, edge_sf_c2);

        scene.add(edge_sr_c2.mesh);
        scene.add(edge_sr_c3.mesh);

      }else if(commonNeighbours.length == 2){

        var edge_sr_c2 = new Edge(commonNeighbours.last, sr);

        var edge_sq_c2 = Edge.isConnected(highestErrorNodes.first, commonNeighbours.last);
        var edge_sf_c2 = Edge.isConnected(highestErrorNodes.last, commonNeighbours.last);

        MFace face = new MFace(edge_sr_sq, edge_sr_c1, edge_sq_c1);
        MFace face1 = new MFace(edge_sr_sq, edge_sr_c2, edge_sq_c2);
        MFace face2 = new MFace(edge_sr_sf, edge_sr_c1, edge_sf_c1);
        MFace face3 = new MFace(edge_sr_sf, edge_sr_c2, edge_sf_c2);

        scene.add(edge_sr_c2.mesh);

        scene.add(face.mesh);
        scene.add(face1.mesh);
        scene.add(face2.mesh);
        scene.add(face3.mesh);

      }else if(commonNeighbours.length == 1){

        MFace face = new MFace(edge_sr_sq, edge_sr_c1, edge_sq_c1);
        MFace face2 = new MFace(edge_sr_sf, edge_sr_c1, edge_sf_c1);
        scene.add(face.mesh);
        scene.add(face2.mesh);

      }else if(commonNeighbours.length < 1){
        throw new StateError("No common neighbours");
      }

      scene.add(edge_sr_sq.mesh);
      scene.add(edge_sr_sf.mesh);
      scene.add(edge_sr_c1.mesh);*/


      /*
      var edge_sr_c1 = new Edge(commonNeighbours.first, sr);
      var edge_sr_c2 = new Edge(commonNeighbours.last, sr);

      var edge_sq_c1 = Edge.isConnected(highestErrorNodes.first, commonNeighbours.first);
      var edge_sq_c2 = Edge.isConnected(highestErrorNodes.first, commonNeighbours.last);

      var edge_sf_c1 = Edge.isConnected(highestErrorNodes.last, commonNeighbours.first);
      var edge_sf_c2 = Edge.isConnected(highestErrorNodes.last, commonNeighbours.last);

      MFace face = new MFace(edge_sr_sq, edge_sr_c1, edge_sq_c1);
      MFace face1 = new MFace(edge_sr_sq, edge_sr_c2, edge_sq_c2);
      MFace face2 = new MFace(edge_sr_sf, edge_sr_c1, edge_sf_c1);
      MFace face3 = new MFace(edge_sr_sf, edge_sr_c2, edge_sf_c2);

      scene.add(face.mesh);
      scene.add(face1.mesh);
      scene.add(face2.mesh);
      scene.add(face3.mesh);*/

      //scene.add(edge_sr_sq.mesh);
      //scene.add(edge_sr_sf.mesh);

      highestErrorNodes.first.errorCounter -= alpha * highestErrorNodes.first.errorCounter;
      highestErrorNodes.last.errorCounter -= alpha * highestErrorNodes.last.errorCounter;

      sr.errorCounter = (highestErrorNodes.first.errorCounter + highestErrorNodes.last.errorCounter) / 2.0;

      numberOfInput = 0;
    }
  }

  void updateErrorVariables(){}

  void createConnectionAndECHL(List<Node> closestPoint){

    var commonNeighbours = Node.getCommonNeighbours(closestPoint.first, closestPoint.last);
    var numberOfCommonNeighbours = commonNeighbours.length;

    Edge connection = Edge.isConnected(closestPoint.first, closestPoint.last);

  if(connection != null){
      connection.age = 0;
    }else{
      if(numberOfCommonNeighbours > 2){
        //Do not do anything
      }else if(numberOfCommonNeighbours == 2){
        var connectionBetweenNeighbours = Edge.isConnected(commonNeighbours.first, commonNeighbours.last);
        if(connectionBetweenNeighbours != null){
          //remove connection and face
          MFace.removeFace(connectionBetweenNeighbours, scene);
          Edge.removeEdge(commonNeighbours.first, commonNeighbours.last, scene);
        }
        var edgeBetweenClosestPoint = new Edge(closestPoint.first, closestPoint.last);
        var edge_s1_n1 = Edge.isConnected(closestPoint.first, commonNeighbours.first);
        var edge_s1_n2 = Edge.isConnected(closestPoint.first, commonNeighbours.last);
        var edge_s2_n1 = Edge.isConnected(closestPoint.last, commonNeighbours.first);
        var edge_s2_n2 = Edge.isConnected(closestPoint.last, commonNeighbours.last);

        scene.add(edgeBetweenClosestPoint.mesh);

        var face_s1_s2_n1 = new MFace(edge_s1_n1, edge_s2_n1, edgeBetweenClosestPoint);
        var face_s1_s2_n2 = new MFace(edge_s1_n2, edge_s2_n2, edgeBetweenClosestPoint);
        scene.add(face_s1_s2_n1.mesh);
        scene.add(face_s1_s2_n2.mesh);

      }else if(numberOfCommonNeighbours == 1){
        var edgeBetweenClosestPoint = new Edge(closestPoint.first, closestPoint.last);
        var edge_s1_n1 = Edge.isConnected(closestPoint.first, commonNeighbours.first);
        var edge_s2_n1 = Edge.isConnected(closestPoint.last, commonNeighbours.first);
        var face_s1_s2_n1 = new MFace(edge_s1_n1, edge_s2_n1, edgeBetweenClosestPoint);

        scene.add(edgeBetweenClosestPoint.mesh);
        scene.add(face_s1_s2_n1.mesh);
      }else if(numberOfCommonNeighbours < 1){
        var edgeBetweenClosestPoint = new Edge(closestPoint.first, closestPoint.last);
        scene.add(edgeBetweenClosestPoint.mesh);
      }
    }
  }
  /*
  void createConnectionAndECHL(List<Node> closestPoint){

    //var commonNeighbours = Node.getCommonNeighbours(closestPoint.first, closestPoint.last);
    //var numberOfCommonNeighbours = commonNeighbours.length;

    Edge connection = Edge.isConnected(closestPoint.first, closestPoint.last);

    if(connection != null){
      connection.age = 0;
    }else{
      var edgeBetweenClosestPoint = new Edge(closestPoint.first, closestPoint.last);
      scene.add(edgeBetweenClosestPoint.mesh);
    }




    /*if(connection != null){
      connection.age = 0;
    }else{
      if(numberOfCommonNeighbours > 2){
        //Do not do anything
      }else if(numberOfCommonNeighbours == 2){
        var connectionBetweenNeighbours = Edge.isConnected(commonNeighbours.first, commonNeighbours.last);
        if(connectionBetweenNeighbours != null){
          //remove connection and face
          MFace.removeFace(connectionBetweenNeighbours, scene);
          Edge.removeEdge(commonNeighbours.first, commonNeighbours.last, scene);
        }
        var edgeBetweenClosestPoint = new Edge(closestPoint.first, closestPoint.last);
        var edge_s1_n1 = Edge.isConnected(closestPoint.first, commonNeighbours.first);
        var edge_s1_n2 = Edge.isConnected(closestPoint.first, commonNeighbours.last);
        var edge_s2_n1 = Edge.isConnected(closestPoint.last, commonNeighbours.first);
        var edge_s2_n2 = Edge.isConnected(closestPoint.last, commonNeighbours.last);

        scene.add(edgeBetweenClosestPoint.mesh);

        var face_s1_s2_n1 = new MFace(edge_s1_n1, edge_s2_n1, edgeBetweenClosestPoint);
        var face_s1_s2_n2 = new MFace(edge_s1_n2, edge_s2_n2, edgeBetweenClosestPoint);

      }else if(numberOfCommonNeighbours == 1){
        var edgeBetweenClosestPoint = new Edge(closestPoint.first, closestPoint.last);
        var edge_s1_n1 = Edge.isConnected(closestPoint.first, commonNeighbours.first);
        var edge_s2_n1 = Edge.isConnected(closestPoint.last, commonNeighbours.first);
        var face_s1_s2_n1 = new MFace(edge_s1_n1, edge_s2_n1, edgeBetweenClosestPoint);

        scene.add(edgeBetweenClosestPoint.mesh);
      }else if(numberOfCommonNeighbours < 1){
        var edgeBetweenClosestPoint = new Edge(closestPoint.first, closestPoint.last);
        scene.add(edgeBetweenClosestPoint.mesh);
      }
    }*/
  }
*/
  void createConnectionAndECHLThree(List<Node> closestPoint){
    Edge c1 = createConnection(closestPoint[0], closestPoint[1]);
    Edge c2 = createConnection(closestPoint[0], closestPoint[2]);
    Edge c3 = createConnection(closestPoint[1], closestPoint[2]);

    var face = MFace.isFormFace(c1, c2, c3);
    if(face == null){
      face = new MFace(c1, c2, c3);
      scene.add(face.mesh);
    }
  }

  void fillQuads(){
    List listOfAllEdges = Edge.getAllEdge(map);

    var oneFaceEdges = new List<Edge>();
    for(Edge e in listOfAllEdges){
      if(e.faces.length < 2){
        oneFaceEdges.add(e);
      }
    }

    List<Route> foundRoutes = new List();
    for(Edge e in oneFaceEdges){
      var connectedEdge = lookAllConnectedEdge(e, oneFaceEdges);
      for(Route newRoute in connectedEdge){
        if(!foundRoutes.contains(newRoute)){
            if(newRoute.edges.length == 4){
              var result = getLongestEdge(newRoute.edges[0], newRoute.edges[2]);
              scene.add(result.mesh);

              var a = result.start;
              var b = result.end;

              if(newRoute.edges[1].start == a || newRoute.edges[1].end == a){
                if(newRoute.edges[2].start == a || newRoute.edges[2].end == a){
                  var faceOne = new MFace(newRoute.edges[1], newRoute.edges[0], result);
                  var faceTwo = new MFace(newRoute.edges[2], newRoute.edges[3], result);
                  scene.add(faceOne.mesh);
                  scene.add(faceTwo.mesh);
                }else{
                  var faceOne = new MFace(newRoute.edges[1], newRoute.edges[2], result);
                  var faceTwo = new MFace(newRoute.edges[0], newRoute.edges[3], result);
                  scene.add(faceOne.mesh);
                  scene.add(faceTwo.mesh);
                }
              }else{
                if(newRoute.edges[2].start == b || newRoute.edges[2].end == b){
                  var faceOne = new MFace(newRoute.edges[1], newRoute.edges[0], result);
                  var faceTwo = new MFace(newRoute.edges[2], newRoute.edges[3], result);
                  scene.add(faceOne.mesh);
                  scene.add(faceTwo.mesh);
                }else{
                  var faceOne = new MFace(newRoute.edges[1], newRoute.edges[2], result);
                  var faceTwo = new MFace(newRoute.edges[0], newRoute.edges[3], result);
                  scene.add(faceOne.mesh);
                  scene.add(faceTwo.mesh);
                }
              }
            }
          foundRoutes.add(newRoute);
        }
      }

    }
  }

  Edge getLongestEdge(Edge e, Edge e2){
    List<List<Node>> helper = new List();
    helper.add(new List());
    helper.add(new List());
    if(e.start.weight.distanceTo(e2.start.weight) > e.start.weight.distanceTo(e2.end.weight)){
      helper[0].add(e.start);
      helper[0].add(e2.start);
    }else{
      helper[0].add(e.start);
      helper[0].add(e2.end);
    }

    if(e.end.weight.distanceTo(e2.start.weight) > e.end.weight.distanceTo(e2.end.weight)){
      helper[1].add(e.end);
      helper[1].add(e2.start);
    }else{
      helper[1].add(e.end);
      helper[1].add(e2.end);
    }

    if(helper[0][0].weight.distanceTo(helper[0][1].weight)
        < helper[1][0].weight.distanceTo(helper[1][1].weight)){
      return new Edge(helper[0][0], helper[0][1]);
    }else{
      return new Edge(helper[1][0], helper[1][1]);
    }
  }

  List<Route> lookAllConnectedEdge(Edge e, List oneFaceEdge){
    allRoute = new List<List<Edge>>();
    var start = e.end;
    getRoute(e, start, oneFaceEdge, new List());
    var retVal = new List<Route>();
    for(List<Edge> route in allRoute){
      retVal.add(new Route(route));
    }
    return retVal;
  }
  List<List<Edge>> allRoute = new List<List<Edge>>();
  void getRoute(Edge lastEdge, Node start, List oneFaceEdge, List<Edge> route){
    var possibleNextEdge = new List<Edge>();
    possibleNextEdge.addAll(start.getEdgeFromList(oneFaceEdge));
    possibleNextEdge.remove(lastEdge);
    for(Edge e in possibleNextEdge){
      print("Before: ${route.length}");
      route.add(e);
      print("After: ${route.length}");
      if(route.first != route.last && Edge.isRoute(route)){
        allRoute.add(new List.from(route));
        print("${route.length}");
      }else{
        if(route.length < 4) {
          start = e.end == start ? e.start : e.end;
          getRoute(e, start, oneFaceEdge, new List.from(route, growable: true));
        }else{
          route.remove(e);
        }
      }
    }
  }

  Edge createConnection(Node a, Node b){
    var connection = Edge.isConnected(a,b);
    if(connection != null){
      connection.age = 0;
      if(!scene.children.contains(connection.mesh)){
        connection.mesh.geometry.verticesNeedUpdate;
        scene.add(connection.mesh);
      }
    }else{
      connection = new Edge(a, b);
      scene.add(connection.mesh);
    }
    return connection;
  }

  List<Node> findTheThreeBestMatchingNode(Vector3 epsilon){
    var min = 99999997;
    var min2 = 99999998;
    var min3 = 99999999;

    var s1, s2, s3;

    for(var i = 0; i < map.length; i++){
      var actDist = map[i].weight.distanceTo(epsilon);
      if(actDist < min){
        min3 = min2;
        s3 = s2;
        min2 = min;
        s2 = s1;
        s1 = map[i];
        min = actDist;
      }else if(actDist < min2){
        min3 = min2;
        s3 = s2;
        s2 = map[i];
        min2 = actDist;
      }else if(actDist < min3){
        s3 = map[i];
        min3 = actDist;
      }
    }
    return [s1,s2,s3];
  }

  List<Node> findTheTwoBestMatchingNode(Vector3 epsilon){
    var a = map[0], b = map[1];
    var s1, s2;
    var distOne = a.weight.distanceTo(epsilon);
    var distTwo = b.weight.distanceTo(epsilon);
    var minDist, minSecondDist;
    if(distOne < distTwo){
      s1 = a;
      s2 = b;
      minDist = distOne;
      minSecondDist = distTwo;
    }else{
      s1 = b;
      s2 = a;
      minDist = distTwo;
      minSecondDist = distOne;
    }

    for(var i = 2; i < map.length; i++){
      var actDist = map[i].weight.distanceTo(epsilon);
      if(actDist < minDist){
        s2 = s1;
        minSecondDist = minDist;
        s1 = map[i];
        minDist = actDist;
      }else if(actDist < minSecondDist){
        s2 = map[i];
        minSecondDist = actDist;
      }
    }
    return [s1, s2];
  }
}