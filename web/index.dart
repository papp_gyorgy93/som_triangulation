library simulation;

import 'dart:html';
import 'dart:math' as Math;
import 'package:vector_math/vector_math.dart';
import 'package:three/three.dart';
import "package:three/extras/scene_utils.dart";
import "package:three/extras/controls/trackball_controls.dart";
import "package:rl_matrix/rl_matrix.dart";
import 'dart:convert' show HtmlEscape;

/*part "circle.dart";
part "sphere.dart";
part "plane_surface.dart";
part "line.dart";
part "line_3d.dart";
part "line_2d.dart";
part "rigidObj.dart";*/
part "node.dart";
part "som.dart";
part "edge.dart";
part "mface.dart";
part "route.dart";
//part "coordinatePolar.dart";

Element container;

PerspectiveCamera camera;
Scene scene;
WebGLRenderer renderer;
TrackballControls controls;

//List<Circle> balls;
//List<Sphere> sphere;

Som som;

bool nextStepIteration;
bool onlyOnce = false;

render() {
  renderer.render(scene, camera);
}

void createRender(){
  container = new Element.tag('div');
  document.body.nodes.add(container);

  camera = new PerspectiveCamera(45.0, window.innerWidth / window.innerHeight, 1.0, 2000.00);

  scene = new Scene();

  scene.add(new AmbientLight(0x404040));

  var geom = new Geometry();
  geom.vertices.add(new Vector3(0.0,0.0,0.0));
  geom.vertices.add(new Vector3(100.0,0.0,0.0));
  //var mesh = new Line(geom);

  //scene.add(mesh);

  renderer = new WebGLRenderer(devicePixelRatio: window.devicePixelRatio.toDouble())
    ..setClearColorHex(0xE9FFE1, 1);
  renderer.setSize(window.innerWidth, window.innerHeight);

  container.nodes.add(renderer.domElement);

  window.onResize.listen(onWindowResize);

  camera.position = new Vector3(0.0,0.0,300.0);

  camera.lookAt(new Vector3(0.0,0.0,0.0));

  controls = new TrackballControls(camera);

  controls.rotateSpeed = 1.0;
  controls.zoomSpeed = 1.2;
  controls.panSpeed = 0.8;

  controls.noZoom = false;
  controls.noPan = false;

  controls.staticMoving = true;
  controls.dynamicDampingFactor = 0.3;

  controls.keys = [65, 83, 68];

  controls.addEventListener('change', (_) => render());

  window.onKeyPress.listen(myKeyDownEvent);

  document.querySelector("#start").onClick.listen((Event e){
    nextStepIteration = true;
    if(!som.isLearning()){
      waitForQuadFill = false;
    }
  });

  document.querySelector("#stop").onClick.listen((Event e){
    nextStepIteration = false;
  });

  document.querySelector("#next").onClick.listen((Event e){
    nextStepIteration = true;
    onlyOnce = true;
    if(!som.isLearning()){
      waitForQuadFill = false;
    }
  });

  nextStepIteration = false;
}

void myKeyDownEvent(Event event) {
  if (event is KeyboardEvent) {
    KeyboardEvent kevent = event;
    switch (kevent.keyCode) {
      case 32:
        nextStepIteration = true;
        if(!som.isLearning()){
          waitForQuadFill = false;
        }
        break;
    }
  }
}

void _onDragOver(MouseEvent event) {
  event.stopPropagation();
  event.preventDefault();
  event.dataTransfer.dropEffect = 'copy';
}

_addPoints(){

  //var a = new ParticleSystem(new Geometry()..vertices.add(new Vector3(0.0,0.0,0.0)),new ParticleBasicMaterial(color: 0xff0000, size: 15));
  var geometry = new Geometry();
  for(Vector3 point in points){

    geometry.vertices.add(point);

  }
  scene.add(new ParticleSystem(geometry, new ParticleBasicMaterial(color: 0x000000, size: 2))..name = "input");
}

_readThePoints(FileReader fr){
  //print(fr.result);
  String wholeDoc = fr.result;
  var lines = wholeDoc.split("\n");
  int startIndex = 0;
  var stopIndex = 0;
  while(startIndex < lines.length && lines[startIndex].compareTo("end_header") != 0){
    var split = lines[startIndex].split(" ");
    if(split.length > 2
        && split[0].compareTo("element") == 0
        && split[1].compareTo("vertex") == 0){
      stopIndex = int.parse(split[2]);
    }

    startIndex++;

  }
  stopIndex += startIndex;
  startIndex++;
  //points.clear();
  for(var i = startIndex; i < stopIndex; i++){
    var coordinates = lines[i].split(" ");
    points.add(new Vector3(
        double.parse(coordinates[0]),
        double.parse(coordinates[1]),
        double.parse(coordinates[2])
    )
    );
  }
  _addPoints();

}

void _onFilesSelected(List<File> files) {
  var list = new Element.tag('ul');
  for (File file in files) {
    FileReader fr = new FileReader();
    fr.onLoad.listen((fileEvent) => _readThePoints(fr));
    fr.onError.listen((evt) => print("error ${fr.error.code}"));
    fr.readAsText(file);
  }
}

void _onDrop(MouseEvent event) {
  event.stopPropagation();
  event.preventDefault();
  _dropZone.classes.remove('hover');
  _onFilesSelected(event.dataTransfer.files);
}

Element _dropZone;
List<Vector3> points;



void init() {

  createRender();

  //_dropZone = document.querySelector('#drop-zone');
  //_dropZone.onDragOver.listen(_onDragOver);
  //_dropZone.onDragEnter.listen((e) => _dropZone.classes.add('hover'));
  //_dropZone.onDragLeave.listen((e) => _dropZone.classes.remove('hover'));
  //_dropZone.onDrop.listen(_onDrop);


  //var a = new ParticleSystem(new Geometry()..vertices.add(new Vector3(0.0,0.0,0.0)),new ParticleBasicMaterial(color: 0xff0000, size: 15));
  //scene.add(a);
  //scene.add(a);
  points = new List<Vector3>();
  for(var i = 0; i <= 2 * Math.PI; i+=0.1){
    for(var j = 0; j <= Math.PI; j+=0.1) {
      points.add(new Vector3(
          50 * Math.cos(i) * Math.sin(j),
          50 * Math.sin(i) * Math.sin(j),
          50 * Math.cos(j)
      ));

    }
  }

  /*points.add(new Vector3(50.0,50.0,-50.0));
  points.add(new Vector3(-50.0,50.0,-50.0));
  points.add(new Vector3(-50.0,-50.0,-50.0));
  points.add(new Vector3(-25.0,0.0,50.0));
  points.add(new Vector3(0.0,0.0,-50.0));
  points.add(new Vector3(-25.0,25.0,25.0));*/


  /*points.add(new Vector3(50.0,50.0,50.0));
  points.add(new Vector3(-50.0,50.0,50.0));
  points.add(new Vector3(-50.0,-50.0,50.0));
  points.add(new Vector3(50.0,-50.0,50.0));*/

  /*points.add(new Vector3(100.0,-40.0,25.0));
  points.add(new Vector3(0.0,50.0,0.0));
  points.add(new Vector3(0.0,0.0,50.0));
  points.add(new Vector3(0.0,0.0,-50.0));
  points.add(new Vector3(-50.0,-50.0,0.0));
  points.add(new Vector3(50.0,-50.0,0.0));*/

  /*points.add(new Vector3(24.565690994262695,12.919843673706055,23.20740509033203));
  points.add(new Vector3(12.023628234863281,34.209346771240234,11.552873611450195));
  points.add(new Vector3(4.868988037109375,29.393199920654297,45.177894592285156));
  points.add(new Vector3(9.00919246673584,8.083086967468262,8.397966384887695));
  points.add(new Vector3(28.888137817382812,31.719215393066406,25.337921142578125));
  points.add(new Vector3(40.42325973510742,10.776947975158691,6.554034233093262));*/

  /*var rand = new Math.Random();

  for(var j = 0; j < 10; j++) {
    points.add(new Vector3(
        (rand.nextDouble() * 200) - 100,
        (rand.nextDouble() * 200) - 100,
        (rand.nextDouble() * 200) - 100
    ));
    //print(points.last);
  }*/

  _addPoints();


  som = new Som(points, scene);

  //som.buildFaceCreation();

  var test = 0;

}

onWindowResize(event) {

  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize(window.innerWidth, window.innerHeight);
}

bool waitForQuadFill = true;

update(){

  if(nextStepIteration){
    if(onlyOnce){
      nextStepIteration = false;
      onlyOnce = false;
    }
    if(som.isLearning()){
      som.learning();
    }else{
      if(!waitForQuadFill){
        som.fillQuads();
        waitForQuadFill = true;
      }else{
        nextStepIteration = false;
      }
    }
  }

  controls.update();
}

animate(num time) {
  window.requestAnimationFrame(animate);
  render();
  update();
}

void main() {
  init();
  animate(0);
}


/*
* PlaneSurface a = new PlaneSurface.fromNormalAndPoint(
      new Vector3(-0.74, -0.15, 1.85),
      new Vector3(-0.44, 1.08, 1.29));
  PlaneSurface b = new PlaneSurface.fromNormalAndPoint(
      new Vector3(-0.68, -1.22, -0.26),
      new Vector3(0.12, 2.24, 4.0));
  PlaneSurface c = new PlaneSurface.fromNormalAndPoint(
      new Vector3(-1.45, 0.33, 0.21),
      new Vector3(2.72, -3.99, 4.0));

  Vector3 res = PlaneSurface.threePlaneIntersection(a, b, c);



  Voronoi voronoi = new Voronoi(points, (List<Vector3> newPoints, int color){
    var geometry = new Geometry();
    for(Vector3 point in newPoints){
      geometry.vertices.add(point);
    }
    scene.add(new ParticleSystem(geometry, new ParticleBasicMaterial(color: color, size: 5)));
  });

  for(PlaneSurface plane in voronoi.vPoints[10].listOfPlanes){
    var planeGeom = plane.getGeometry();
    var obj = createMultiMaterialObject( planeGeom, [
      new MeshBasicMaterial(side: DoubleSide, transparent: true, opacity: 0.2),
      new MeshBasicMaterial(color: 0x000000 , wireframe: true)])..isDynamic = true;
    scene.add(obj);
  }

  var point = new ParticleSystem(new Geometry()..vertices.add(voronoi.vPoints[10].point),new ParticleBasicMaterial(color: 0xff0000, size: 5));
  scene.add(point);


  Delaunay delaunay = new Delaunay(voronoi.vPoints);
  delaunay.makeTriangulation();

  /*var obj = delaunay.tetrahedrons[0].getObject();
  scene.add(obj);*/
  /*var obj = delaunay.tetrahedrons[1].getObject();
  scene.add(obj);
  obj = delaunay.tetrahedrons[3].getObject();
  scene.add(obj);*/

  for(Tetrahedron tetrahedron in delaunay.tetrahedrons){
    var obj = tetrahedron.getObject();
    scene.add(obj);
  }

  //scene.add(delaunay.tetrahedrons[0].getObject());
  //scene.add(delaunay.tetrahedrons[1].getObject());
  //scene.add(delaunay.tetrahedrons[2].getObject());
  //scene.add(delaunay.tetrahedrons[3].getObject());


  /*for(VoronoiPoint p in voronoi.vPoints){
    p.listOfIntersectionPoint.forEach((IntersectionPoint ip){
      var point = new ParticleSystem(new Geometry()..vertices.add(ip.point),new ParticleBasicMaterial(color: 0xff0000, size: 5));
      scene.add(point);
    });
  }*/

  /*delaunay.points[10].listOfIntersectionPoint.forEach((IntersectionPoint ip){
    var point = new ParticleSystem(new Geometry()..vertices.add(ip.point),new ParticleBasicMaterial(color: 0xff0000, size: 5));
    scene.add(point);
  });*/
*/