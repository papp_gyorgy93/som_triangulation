part of simulation;



class Node{

  Vector3 weight;
  double errorCounter = 0.0;
  List<Edge> listOfEdges;
  Object3D mesh;

  Node(this.weight){
    var geom = new Geometry();
    geom.vertices.add(weight);
    geom.isDynamic = true;
    geom.verticesNeedUpdate = true;
    listOfEdges = new List();
    mesh = new ParticleSystem(geom, new ParticleBasicMaterial(color: 0xff0000, size: 5));
    mesh.isDynamic = true;
  }

  void updateErrorCounter(Vector3 epsilon){
    errorCounter = errorCounter + Math.pow((weight - epsilon).length, 2);
    //errorCounter = (weight - epsilon).length2;
  }

  void updateErrorVariable(double beta){
    this.errorCounter -= beta * this.errorCounter;
  }

  void moveTowards(Vector3 epsilon, double eb, double en){
    this.weight = this.weight + (epsilon - this.weight).scale(eb);
    var neighbours = getNeighbours();
    for(Node n in neighbours){
      n.weight = n.weight + (epsilon - n.weight).scale(en);
    }
  }

  void updateMesh(){
    this.mesh.geometry.vertices[0] = weight;
    this.mesh.geometry.verticesNeedUpdate = true;

    for(Edge e in listOfEdges){
      e.update();
      if(e.faces != null && e.faces.length > 0){
        e.faces.forEach((MFace f){f.update();});
      }
    }
  }

  List<Node> get neighbours{
    return [];
  }

  void updateAges(){
    for(Edge e in listOfEdges){
      e.updateAge();
    }
  }

  List<Node> getNeighbours(){
    var retVal = new List<Node>();
    for(Edge e in this.listOfEdges){
      if(e.end == this){
        retVal.add(e.start);
      }else{
        retVal.add(e.end);
      }
    }
    return retVal;
  }

  List<Edge> getEdgeFromList(List oneFaceEdge){
    var retVal = new List();
    for(Edge e in this.listOfEdges){
      if(oneFaceEdge.contains(e)){
        retVal.add(e);
      }
    }
    return retVal;
  }

  static List<Node> getCommonNeighbours(Node a, Node b){
    var listOfNeighboursA = a.getNeighbours();
    var listOfNeighboursB = b.getNeighbours();

    var retVal = new List<Node>();
    for(Node n in listOfNeighboursA){
      if(listOfNeighboursB.contains(n)){
        retVal.add(n);
      }
    }
    return retVal;
  }

  static List<Node> getHighestErrorNodes(List<Node> allNodes){
    var highestErrorNode = allNodes.first;
    for(var i = 1; i < allNodes.length; i++){
      if(allNodes[i].errorCounter > highestErrorNode.errorCounter){
        highestErrorNode = allNodes[i];
      }
    }

    var highestErrorNodeNeighbours = highestErrorNode.getNeighbours();
    var highestErrorNeighbours = highestErrorNodeNeighbours.first;
    for(var i = 1; i < highestErrorNodeNeighbours.length; i++){
      if(highestErrorNodeNeighbours[i].errorCounter > highestErrorNeighbours.errorCounter){
        highestErrorNeighbours = highestErrorNodeNeighbours[i];
      }
    }

    return [highestErrorNode, highestErrorNeighbours];
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    return other is Node &&
        this.weight == other.weight;
  }

  @override
  int get hashCode {
    return weight.hashCode;
  }
}