part of simulation;

class MFace{
  List<Edge> edges;
  Object3D mesh;

  MFace._fromEdge(Edge a, Edge b, Edge c){
    edges = new List();

    edges.add(a);
    edges.add(b);
    edges.add(c);

    var geom = new Geometry();
    geom.vertices.add(a.start.weight.clone());
    geom.vertices.add(a.end.weight.clone());
    geom.vertices.add(geom.vertices.contains(b.start.weight) ? b.end.weight.clone() : b.start.weight.clone());
    geom.faces.add(new Face3(0,2,1));
    geom.isDynamic = true;
    geom.verticesNeedUpdate = true;
    mesh = new Mesh(geom, new MeshBasicMaterial(color: new Math.Random().nextInt(0xffffff), side: DoubleSide));
    mesh.name = "face";
    mesh.isDynamic = true;

    /*var geom = new Geometry();
    geom.vertices.add(new Vector3(0.0,0.0,0.0));
    geom.vertices.add(new Vector3(10.0,0.0,0.0));
    geom.vertices.add(new Vector3(0.0,10.0,10.0));
    geom.faces.add(new Face3(0,1,2));
    geom.isDynamic = true;
    geom.verticesNeedUpdate = true;
    mesh = new Mesh(geom, new MeshBasicMaterial(color: 0xf0f000, side: DoubleSide));
    mesh.name = "face";
    mesh.isDynamic = true;*/

  }

  void update(){
    mesh.geometry.vertices[0] = edges.first.start.weight.clone();
    mesh.geometry.vertices[1] = edges.first.end.weight.clone();
    mesh.geometry.vertices[2] =
    mesh.geometry.vertices[0] == edges[1].start.weight
        || mesh.geometry.vertices[1] == edges[1].start.weight
        ? edges[1].end.weight.clone()
        : edges[1].start.weight.clone();
    mesh.geometry.verticesNeedUpdate = true;
    mesh.isDynamic = true;
  }

  factory MFace(Edge a, Edge b, Edge c){
    var newFace = new MFace._fromEdge(a,b,c);
    addFace(a, newFace);
    addFace(b, newFace);
    addFace(c, newFace);
    return newFace;
  }

  static void addFace(Edge e, MFace f) {
    if (e.faces == null) {
      e.faces = new List();
    }
    e.faces.add(f);
  }

  static removeFace(Edge e, Scene sc){
    var faces = e.faces;
    if(faces == null){
      return;
    }
    for(MFace f in faces.toList()){
      for(Edge edge in f.edges){
        edge.faces.remove(f);

        /*if(edge.faces.length < 1){
          Edge.removeEdge(edge.start, edge.end, scene);
        }*/
      }
      f.edges.clear();
      sc.remove(f.mesh);
    }
  }

  static removeEdgeAllFace(Edge e, Scene sc){
    if(e.faces !=  null && e.faces.length > 1){
      MFace.removeFace(e, sc);
    }else{
      throw new StateError("edge faces is null or length is zero");
    }
  }

  bool isContainEdge(Edge e){
    return edges[0] == e || edges[1] == e || edges[2] == e;
  }

  static MFace isFormFace(Edge a, Edge b, Edge c){
    var retVal = null;
    if(a.faces == null){
      return null;
    }
    for(MFace e in a.faces){
      if(e.isContainEdge(a) && e.isContainEdge(b) && e.isContainEdge(c)){
        retVal = e;
        break;
      }
    }
    return retVal;
  }

}