part of simulation;

class Edge{

  Node start;
  Node end;
  List<MFace> faces;
  Object3D mesh;

  int age = 0;

  Edge._fromPoint(this.start, this.end){
    faces = new List();
    var geom = new Geometry();
    geom.vertices.add(this.start.weight);
    geom.vertices.add(this.end.weight);
    geom.isDynamic = true;
    geom.verticesNeedUpdate = true;
    mesh = new Line(geom, new LineBasicMaterial(color: 0x000000));
    mesh.name = "edge";
    mesh.isDynamic = true;
  }

  void update(){
    mesh.geometry.vertices[0] = this.start.weight;
    mesh.geometry.vertices[1] = this.end.weight;
    mesh.geometry.verticesNeedUpdate = true;
  }

  factory Edge(Node start, Node end){
    var newEdge = new Edge._fromPoint(start, end);
    start.listOfEdges.add(newEdge);
    end.listOfEdges.add(newEdge);
    return newEdge;
  }

  double get length{
    return (start.weight - end.weight).length;
  }

  int get numberOfFace{
    if(faces != null){
      return faces.length;
    }else{
      return 0;
    }
  }

  bool isContainNode(Node a){
    return this.start == a || this.end == a;
  }

  static Edge isConnected(Node a, Node b){
    var retVal = null;
    for(Edge e in a.listOfEdges){
      if(e.isContainNode(b)){
        retVal = e;
        break;
      }
    }
    return retVal;
  }

  static bool isRoute(List<Edge> route){
    var retVal = true;
    var testRoute = new List.from(route, growable: true);
    testRoute.add(route.first);
    for(var i = 0; i < testRoute.length-2; i++){
      if(!Edge.isContinuous(testRoute[i], testRoute[i+1], testRoute[i+2])) {
        retVal = false;
      }
    }
    return retVal;
  }

  static bool isContinuous(Edge e, Edge e2, Edge e3){
    var retVal = false;
    if(e.start == e2.start || e.start == e2.end){
      if(e.start == e2.start){
        if(e2.end == e3.start || e2.end == e3.end){
          retVal = true;
        }
      }else{
        if(e2.start == e3.start || e2.start == e3.end){
          retVal = true;
        }
      }
    }else if((e.end == e2.start || e.end == e2.end)){
      if(e.end == e2.start){
        if(e2.end == e3.start || e2.end == e3.end){
          retVal = true;
        }
      }else{
        if(e2.start == e3.start || e2.start == e3.end){
          retVal = true;
        }
      }
    }
    return retVal;
    return (e.start == e2.start || e.start == e2.end)
        || (e.end == e2.start || e.end == e2.end);
  }

  static void removeEdge(Node a, Node b, Scene sc){
    Edge removingEdge = null;
    for(Edge e in a.listOfEdges){
      if(e.isContainNode(b)){
        removingEdge = e;
      }
    }
    if(removingEdge != null){
      sc.remove(removingEdge.mesh);
      a.listOfEdges.remove(removingEdge);
      b.listOfEdges.remove(removingEdge);
    }

  }

  void updateAge(){
    age++;
  }

  static List<Edge> getAllEdge(List<Node> allNodes){
    var retVal = new List<Edge>();
    for(Node n in allNodes){
      for(Edge e in n.listOfEdges){
        if(!retVal.contains(e)){
          retVal.add(e);
        }
      }
    }
    return retVal;
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    return other is Edge &&
        other.isContainNode(start) && other.isContainNode(end);
  }

  @override
  int get hashCode {
    return start.hashCode ^ end.hashCode;
  }


}